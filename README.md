# Ionic E-commerce 

## Instructions

Welcome to the tech test phase! In order to advance to the next phase, you should create a simple e-commerce app for Android and iOS using Ionic and Cordova (do not use Capacitor). The interface proposed is at this [Figma](https://www.figma.com/file/TvOrzcblD1w2n3OSYRhtM1/PBHouse), you should basically create a login screen that leads to the market screen listing the products. Use your creativity!

You should use the mock server provided in this repository at the folder `server`, as the data API for the app (using the [json-server](https://www.npmjs.com/package/json-server) npm package). In order to run put the mock server on, execute the npm install in `/server`, after, you should execute the command `npm run serve`.

Fork this git repo, start coding (let me see how you work your branches and commits) and as you finish your work, send the link of you repo to `pbhouse.dp@gmail.com`.

Good test!
