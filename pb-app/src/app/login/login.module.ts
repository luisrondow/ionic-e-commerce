import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginPage } from './login.page';

import { LoginPageRoutingModule } from './login-routing.module';
import { UserService } from 'src/providers/user';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, LoginPageRoutingModule],
  declarations: [LoginPage],
  providers: [UserService],
})
export class LoginPageModule {}
