import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { UserService } from 'src/providers/user';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {
  showPassword = false;
  user: { email: string; password: string } = {
    email: '',
    password: '',
  };

  isKeyboardShown = false;

  constructor(
    public navCtrl: NavController,
    public userService: UserService,
    private alertController: AlertController
  ) {}

  async login() {
    const isUserLogged = await this.userService.login(
      this.user.email,
      this.user.password
    );

    if (isUserLogged) {
      this.navCtrl.navigateRoot('/tabs/tab1');
    } else {
      const alert = await this.alertController.create({
        header: 'Login failed',
        message: 'Please check your credentials',
        buttons: ['OK'],
      });
      await alert.present();
    }
  }

  signup() {
    console.log('Sign up');
  }
}
