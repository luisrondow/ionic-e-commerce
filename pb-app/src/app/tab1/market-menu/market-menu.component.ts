import { Component } from '@angular/core';

@Component({
  selector: 'app-market-menu',
  templateUrl: './market-menu.component.html',
  styleUrls: ['market-menu.component.scss'],
  providers: [],
})
export class MarketMenuComponent {
  menu = [
    {
      description: 'Ofertas',
      image: '../../assets/img/logo.png',
    },
    {
      description: 'Hortifruti',
      image: '../../assets/img/hortifruti.png',
    },
    {
      description: 'Padaria',
      image: '../../assets/img/padaria.png',
    },
    {
      description: 'Açougue',
      image: '../../assets/img/acougue.png',
    },
  ];

  constructor() {}
}
