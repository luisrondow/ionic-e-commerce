import { Component, Input } from '@angular/core';
import { Product } from 'src/types/Product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['product-list.component.scss'],
  providers: [],
})
export class ProductListComponent {
  @Input() products: Product[];

  constructor() {}
}
