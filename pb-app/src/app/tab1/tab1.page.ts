import { Component, OnInit } from '@angular/core';
import { Api } from 'src/providers/api';
import { Product } from 'src/types/Product';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  cartCount = 0;

  market = {
    name: 'Preço Baixo',
    openingHour: '9:00',
    closingHour: '18:00',
    address: '123 Main St',
    city: 'New York',
    state: 'NY',
  };

  marketDistance = 0;
  isMarketNearby = false;

  products: Product[] = [];

  constructor(private api: Api) {}

  ngOnInit() {
    this.cartCount = this.getRandomInt(1, 10);
    this.marketDistance = this.getRandomInt(1, 50);

    this.isMarketNearby = this.marketDistance <= 10;

    this.api.get('products').subscribe((res: any) => {
      this.products = res;
    });
  }

  isOpen() {
    const openingHour = this.market.openingHour.split(':').join('');
    const closingHour = this.market.closingHour.split(':').join('');

    const now = new Date();
    const serializedOpeningTime = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      Number(openingHour[0]),
      Number(openingHour[1]),
      Number(openingHour[2])
    );
    const serializedClosingTime = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      Number(closingHour[0]),
      Number(closingHour[1]),
      Number(closingHour[2])
    );

    return now > serializedOpeningTime && now < serializedClosingTime;
  }

  private getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
