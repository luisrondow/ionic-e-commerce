import { Injectable } from '@angular/core';
import { Api } from './api';

@Injectable()
export class UserService {
  constructor(private api: Api) {}

  async login(email: string, password: string) {
    const users = (await this.api.get('users').toPromise()) as unknown as any[];

    const isUserExists = users.find((user) => {
      if (user.email === email && user.password === password) {
        localStorage.setItem('user', JSON.stringify(user));
        return true;
      }
    });

    return !!isUserExists;
  }
}
